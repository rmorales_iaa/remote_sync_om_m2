#!/bin/bash  -l
#add -l to get the environment variables
#https://stackoverflow.com/questions/2229825/where-can-i-set-environment-variables-that-crontab-will-use
#------------------------------------------------------------------------------
#Variable definition
#------------------------------------------------------------------------------
#global variables
#------------------------------------------------------------------------------
#one of the next two variables must be empty
#OM_STARTING_DATE=2022-01-01
OM_STARTING_DATE=           

OM_ELAPSED_DAY_COUNT=100
#OM_ELAPSED_DAY_COUNT=
#-------------------------------
OM_WORKING_DIR=/home/rafa/proyecto/om/deploy/
OM_OUTPUT_FILE=output/remote_file_to_sync
#------------------------------------------------------------------------------
#set -x	  #activate debugging	
#------------------------------------------------------------------------------
function initial_actions {
  startTime=$(date +'%s')
  echo "========================================================================="
  echo $(date +"%Y-%m-%d:%Hh:%Mm:%Ss") 'Starting m2'
  echo "========================================================================="
}
#------------------------------------------------------------------------------
function final_actions {
  echo "========================================================================="
  echo $(date +"%Y-%m-%d:%Hh:%Mm:%Ss") 'End of m2'
  echo "========================================================================="
  echo "Elapsed time: $(($(date +'%s') - $startTime))s"
}
#------------------------------------------------------------------------------
initial_actions
  /home/rafa/proyecto/remote_sync_om_m2/find_remote_dir_not_cal.bash
  /home/rafa/proyecto/remote_sync_om_m2/remote_sync_om_m2.bash
final_actions
#------------------------------------------------------------------------------
#end of script
