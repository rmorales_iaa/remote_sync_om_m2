#!/bin/bash
#------------------------------------------------------------------------------
#Variable definition
#------------------------------------------------------------------------------
#global variables
#-------------------------------
#OM
OM_WORKING_DIR=/home/rafa/proyecto/om/deploy/
OM_OUTPUT_FILE=/home/rafa/proyecto/om/output/remote_file_to_sync
#-------------------------------
#database
DATABASE_BACKUP_ROOT_DIR=""/home/rafa/uxmal/backup/db_backup/
#------------------------------------------------------------------------------
#backups
DATABASE_BACKUP_COPIES_DIR=$DATABASE_BACKUP_ROOT_DIR/last_backup
POSGRE_BACKCUP_NAME=$DATABASE_BACKUP_ROOT_DIR/$(date +"%Y-%m-%d:%Hh:%Mm:%Ss")_postgre.dump 
MONGO_BACKCUP_NAME=$DATABASE_BACKUP_ROOT_DIR/$(date +"%Y-%m-%d:%Hh:%Mm:%Ss")_mongo.zip
#------------------------------------------------------------------------------
#set -x	  #activate debugging
#------------------------------------------------------------------------------
function initial_actions {
  startTime=$(date +'%s')
  echo "========================================================================="
  echo $(date +"%Y-%m-%d:%Hh:%Mm:%Ss") 'Starting remote_sync_om_m2'
  echo "========================================================================="
}
#------------------------------------------------------------------------------
function final_actions {

  if [ -f "$OM_OUTPUT_FILE" ]; then
    echo "Removing the file with the directories to sync: '$OM_OUTPUT_FILE'"
    rm $OM_OUTPUT_FILE*
  fi
  echo "========================================================================="
  echo $(date +"%Y-%m-%d:%Hh:%Mm:%Ss") 'End of remote_sync_om_m2'
  echo "========================================================================="
  echo "Elapsed time: $(($(date +'%s') - $startTime))s"
}
#------------------------------------------------------------------------------
function remove_old_backup {
  echo "Removing old back up in: '$DATABASE_BACKUP_ROOT_DIR'" 
  rm -fr $DATABASE_BACKUP_ROOT_DIR/*.zip
  rm -fr $DATABASE_BACKUP_ROOT_DIR/*.dump
}
#------------------------------------------------------------------------------
function backup_database_postgre {

  if [ ! -d "$DATABASE_BACKUP_ROOT_DIR" ] 
  then
    echo "Creating directory '$DATABASE_BACKUP_ROOT_DIR'" 
  fi
   
  echo "Creating a backup of the postgre database"
  pg_dump  -Fc --username=rafa  --host=localhost  --blobs --create \
    --dbname=om_cal --file=$POSGRE_BACKCUP_NAME    
}
#------------------------------------------------------------------------------
function backup_database_mongo {

  if [ ! -d "$DATABASE_BACKUP_ROOT_DIR" ] 
  then
    echo "Creating directory '$DATABASE_BACKUP_ROOT_DIR'" 
  fi
   
  echo "Creating a backup of the mongo database"
  
  mongodump --host 161.111.164.199 --authenticationDatabase admin  --username rafa --password rafaMongoIAA  --db astrometry  --gzip --archive=$MONGO_BACKCUP_NAME
     
}
#------------------------------------------------------------------------------
function sync_calibrate_and_astrometry {
  echo "Synchronizating and calibrating directories" 
  
  if ! [ -f "$OM_OUTPUT_FILE" ]; then
    echo "Output file with the remote directories to sync does not exist" 
    return
  fi

  cd $OM_WORKING_DIR        
  while read REMOTE_DIR; do 
  
    echo "Synchronizing and calibrate:'$REMOTE_DIR'"  
    
    dropcaches_system_ctl      
    YEAR=$(date +%Y)

     java -jar om.jar \
      --database-table om_cal:$YEAR \
      --remote-sync-cal $REMOTE_DIR     
        
  done < "$OM_OUTPUT_FILE"
}

#------------------------------------------------------------------------------
function copy_backup {

 if ! [ -d "$DATABASE_BACKUP_COPIES_DIR" ]
 then
     mkdir $DATABASE_BACKUP_COPIES_DIR   
 fi
 cp $POSGRE_BACKCUP_NAME $DATABASE_BACKUP_COPIES_DIR/last_postgre_backup.dump
 cp $MONGO_BACKCUP_NAME  $DATABASE_BACKUP_COPIES_DIR/last_mongo_backup.zip
}
#------------------------------------------------------------------------------

initial_actions
  
  #remove_old_backup
  #backup_database_postgre
  #backup_database_mongo
  #copy_backup
  
  sync_calibrate_and_astrometry
  
final_actions

#------------------------------------------------------------------------------
#end of script
