#!/bin/bash
#------------------------------------------------------------------------------
set -x #debug
#------------------------------------------------------------------------------
#Variable definition
#------------------------------------------------------------------------------

YEAR=$(date +%Y)

#one of the next two variables must be empty
#OM_STARTING_DATE=$YEAR-01-01
OM_STARTING_DATE=           

OM_ELAPSED_DAY_COUNT=150
#OM_ELAPSED_DAY_COUNT=
#-------------------------------
OM_WORKING_DIR=/home/rafa/proyecto/om/deploy/
OM_OUTPUT_FILE=output/remote_file_to_sync
#------------------------------------------------------------------------------
set -x	  #activate debugging	
#------------------------------------------------------------------------------
function initial_actions {
  startTime=$(date +'%s')
  echo "========================================================================="
  echo $(date +"%Y-%m-%d:%Hh:%Mm:%Ss") 'Starting m2'
  echo "========================================================================="
}
#------------------------------------------------------------------------------
function final_actions {
  echo "========================================================================="
  echo $(date +"%Y-%m-%d:%Hh:%Mm:%Ss") 'End of m2'
  echo "========================================================================="
  echo "Elapsed time: $(($(date +'%s') - $startTime))s"
}
#------------------------------------------------------------------------------
function find_remote_dir_not_cal {
  echo "Finding remote files without calibration" 
  dropcaches_system_ctl

  #select the parameter to use
  TIME_PARAMETER=$OM_STARTING_DATE 
  if [ -z "$OM_STARTING_DATE" ];then
    TIME_PARAMETER=$OM_ELAPSED_DAY_COUNT
  fi 

  echo "Using as time parameter:'$TIME_PARAMETER'"
  cd $OM_WORKING_DIR     
  
  java -jar om.jar  \
  --database-table om_cal:$YEAR \
  --remote-dir-not-cal $TIME_PARAMETER  \
  --output-file $OM_OUTPUT_FILE
}

#------------------------------------------------------------------------------
initial_actions
find_remote_dir_not_cal
final_actions
#------------------------------------------------------------------------------
#end of script
